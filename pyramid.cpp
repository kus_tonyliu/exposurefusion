/* pyramid.cpp
** this file is implemetation of pyramid.
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include "UD_sampling.h"
#include "pyramid.h"
#include "math.h"

void check_img(img_info_t src_img)
{
    int sum_img_width = src_img.width;
    int sum_img_height = src_img.height;
    int sum_img_widthbyte = sum_img_width*src_img.channel;

    double val, val1, val2;
    val = val1 = val2 = 0;
    int stop_i, stop_j;
    stop_i = 443;
    stop_j = 37;

    for (int jloop = 0; jloop < sum_img_height; jloop++) {
        for (int iloop = 0; iloop < sum_img_width; iloop++) {
                val = src_img.data[jloop*sum_img_widthbyte + iloop*src_img.channel];
                val1 = src_img.data[jloop*sum_img_widthbyte + iloop*src_img.channel+1];
                val2 = src_img.data[jloop*sum_img_widthbyte + iloop*src_img.channel+2];

            if(iloop == stop_i && jloop == stop_j)
                printf("[check img] x=%d, y=%d, (B,G,R)= (%f, %f, %f)\n", iloop, jloop, val, val1, val2);
        }// iloop
    }// jloop
}

int gaussian_pyramid(img_info_t* src_img, int src_num, img_info_t*** pyr_img, int *pyr_num){
    double nlevel_width, nlevel_height, nlevel_linit;
    int nlevel, ret_code;
    img_info_t *array_pyr;
    img_info_t **n_src_array;
    n_src_array = (img_info_t**)malloc(src_num*sizeof(array_pyr));
    if( n_src_array == NULL){
        printf("memory allocate fail. %s, %d", __FILE__, __LINE__);
        return -1;
    }

    for (int nloop = 0; nloop < src_num; nloop++) {
        nlevel_width = log2(src_img[nloop].width);
        nlevel_height= log2(src_img[nloop].height);
        nlevel_linit = log2(MIN_PYRAMID_SIZE);
        nlevel = floor(MINIMUM(nlevel_width,nlevel_height)) - floor(nlevel_linit);
        if (nlevel < 0)
            return -1;

        // allocat pyramid buff
        array_pyr = (img_info_t *)malloc(sizeof(img_info_t) * nlevel);
        if( array_pyr == NULL){
            printf("memory allocate fail. %s, %d", __FILE__, __LINE__);
            return -1;
        }

        // copy src to 0 level of pyramid
        memcpy(array_pyr, (src_img+nloop), sizeof(img_info_t));
        // printf("gau: level: %d, width: %d, height: %d\n", 0, array_pyr[0].width,  array_pyr[0].height);
        for (int ilevel = 0; ilevel < nlevel; ilevel++) {
            ret_code = down_sampling(*(array_pyr + ilevel), array_pyr + ilevel + 1);
            if(ret_code < 0){
                return ret_code;
            }
        }
        *(n_src_array+nloop) = array_pyr;
    }
    *pyr_img = n_src_array;
    *pyr_num  = nlevel;
    return ret_code;
}

int laplacian_pyramid(img_info_t** src_pyr_img, int pyr_num, int src_num, img_info_t*** pyr_img)
{
    int ret_code;
    img_info_t expend_img;
    img_info_t* array_pyr, *current_pyr;
    img_info_t **n_src_array;
    n_src_array = (img_info_t**)malloc(src_num*sizeof(array_pyr));
    if( n_src_array == NULL){
        printf("memory allocate fail. %s, %d", __FILE__, __LINE__);
        return -1;
    }

    for (int nloop = 0; nloop < src_num; nloop++) {
        //allocat pyramid buff
        array_pyr = (img_info_t*)malloc(sizeof(img_info_t)*(pyr_num+1));
        if( array_pyr == NULL){
            printf("memory allocate fail. %s, %d", __FILE__, __LINE__);
            return -1;
        }

        //copy highest level from gauss pyr to laplac pyr
        current_pyr = src_pyr_img[nloop];
        memcpy(array_pyr+pyr_num, current_pyr+pyr_num, sizeof(img_info_t));

        for(int ilevel = pyr_num; ilevel > 0; ilevel--){
            ret_code = up_sampling(*(current_pyr+ilevel),  &expend_img);
            if(ret_code < 0){
                return ret_code;
            }
            ret_code = img_subs(*(current_pyr+ilevel-1), expend_img, (array_pyr+ilevel-1));
            if(ret_code < 0){
                return ret_code;
            }
            if(expend_img.data != NULL)
                free(expend_img.data);
        }
        *(n_src_array+nloop) = array_pyr;
    }
    *pyr_img = n_src_array;
    return ret_code;
}

int reconstruct_laplacian_pyramid(img_info_t* src_pyr_img, int pyr_num, img_info_t** pyr_img)
{
    int ret_code;
    //allocat pyramid buff
    img_info_t* array_pyr;
    array_pyr = (img_info_t*)malloc(sizeof(img_info_t)*(pyr_num+1));
    if( array_pyr == NULL){
        printf("memory allocate fail. %s, %d", __FILE__, __LINE__);
        return -1;
    }

    //copy the highest image from laplac to gauss
    memcpy(array_pyr+pyr_num, src_pyr_img+pyr_num, sizeof(img_info_t));

    img_info_t expend_img;
    for(int ilevel = pyr_num; ilevel > 0; ilevel--){
        ret_code = up_sampling(*(array_pyr+ilevel),  &expend_img);
        if(ret_code < 0){
            return ret_code;
        }
        ret_code = img_add(*(src_pyr_img+ilevel-1), expend_img, (array_pyr+ilevel-1));
        if(ret_code < 0){
            return ret_code;
        }
        if(expend_img.data != NULL)
            free(expend_img.data);
    }
    *pyr_img = array_pyr;

    return ret_code;
}

int combine_Nsrc_pyr(img_info_t **wmap_gau_pyr, img_info_t **laplac_pyr, int pyr_num, int src_num, img_info_t **combin_pyr)
{
    int wmap_width, wmap_height, wmap_widthbyte, wmap_nchannel;
    int laplac_width, laplac_height, laplac_widthbyte, laplac_nchannel;
    int final_width, final_height, final_widthbyte, final_nchannel;
    img_info_t *array_pyr, *array_wmap, *array_final_pyr;
    img_info_t *current_pyr, *current_map, *current_final_pry;
    double weight, valR,valG,valB, current_val;

    array_final_pyr = (img_info_t*)malloc((pyr_num+1)*sizeof(img_info_t));
    if( array_final_pyr == NULL){
        printf("memory allocate fail. %s, %d", __FILE__, __LINE__);
        return -1;
    }

    //access N-th src_img
    for(int nsrcloop = 0; nsrcloop < src_num; nsrcloop++){
        array_wmap  = wmap_gau_pyr[nsrcloop];
        array_pyr   = laplac_pyr[nsrcloop];

        //access pyramid N level
        for(int pyrloop = 0; pyrloop < pyr_num+1; pyrloop++){
            current_map         = array_wmap+pyrloop;
            current_pyr         = array_pyr+pyrloop;
            current_final_pry   = array_final_pyr+pyrloop;

            wmap_width          = current_map->width;
            wmap_height         = current_map->height;
            wmap_nchannel       = current_map->channel;
            wmap_widthbyte      = wmap_width*wmap_nchannel;
            laplac_width        = current_pyr->width;
            laplac_height       = current_pyr->height;
            laplac_nchannel     = current_pyr->channel;
            laplac_widthbyte    = laplac_width*laplac_nchannel;

            final_width         = MINIMUM(laplac_width, wmap_width);
            final_height        = MINIMUM(laplac_height, wmap_height);
            final_nchannel      = laplac_nchannel;
            final_widthbyte     = final_width*final_nchannel;

            if(0 == nsrcloop){
                current_final_pry->width = final_width;
                current_final_pry->height = final_height;
                current_final_pry->channel = final_nchannel;
                current_final_pry->data = (double*)malloc(final_height*final_widthbyte*PIXEL_SIZE);
                if (current_final_pry->data == NULL) {
                    printf("memory allocate fail. %s, %d", __FILE__, __LINE__);
                    return -1;
                }
                memset(current_final_pry->data, 0, final_height*final_widthbyte*PIXEL_SIZE);
            }
            //access image
            for(int jloop = 0; jloop < final_height; jloop++){
                for(int iloop = 0; iloop < final_width; iloop++){
                    weight = current_map->data[jloop*wmap_widthbyte + iloop*wmap_nchannel];

                    //B
                    valB         = current_pyr->data[jloop*laplac_widthbyte + iloop*laplac_nchannel];
                    current_val = current_final_pry->data[jloop*final_widthbyte + iloop*final_nchannel];
                    current_final_pry->data[jloop*final_widthbyte + iloop*final_nchannel] = weight * valB + current_val;
                    //G
                    valG         = current_pyr->data[jloop*laplac_widthbyte + iloop*laplac_nchannel + 1];
                    current_val = current_final_pry->data[jloop*final_widthbyte + iloop*final_nchannel + 1];
                    current_final_pry->data[jloop*final_widthbyte + iloop*final_nchannel + 1] = weight * valG + current_val;
                    //R
                    valR         = current_pyr->data[jloop*laplac_widthbyte + iloop*laplac_nchannel + 2];
                    current_val = current_final_pry->data[jloop*final_widthbyte + iloop*final_nchannel + 2];
                    current_final_pry->data[jloop*final_widthbyte + iloop*final_nchannel + 2] = weight * valR + current_val;

                }// iloop
            }// jloop
        }// pyr loop
    }// nsrc loop
    *combin_pyr = array_final_pyr;

    return 0;
}

