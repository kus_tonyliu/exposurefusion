/* common_define.h
**
**
*/
#ifndef _COMMON_DEFINE_H_
#define _COMMON_DEFINE_H_

#define MAXIMUM(x, y)   (x) > (y) ? (x) : (y)
#define MINIMUM(x, y)   (x) < (y) ? (x) : (y)

#define MIN_PYRAMID_SIZE    (8)                 //NxN pixel
#define PIXEL_SIZE          (sizeof(double))    //size of a pixel

typedef struct{
    int width;          //image width
    int height;         //image height
    int channel;        //image depth/channel
    double* data = NULL;       //pointer to image content
}img_info_t;

// filter kernel
/* kernel matrix and scale should be filled with integer.
**       |1 1 1|   
** (1/9)*|1 1 1|
**       |1 1 1|
** size = 3, scale = 9, matrix[] = [1 1 1 1 1 1 1 1 1], 
*/
#define MAX_KERNEL_SIZE         (7)
typedef struct {
    double matrix[MAX_KERNEL_SIZE*MAX_KERNEL_SIZE];
    int size;
    double scale;
} kernel_t;

#endif