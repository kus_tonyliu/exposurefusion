/* convolution.cpp
** this file is implement image convolution with a kernel.
** and basic image processing function.
*/
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include "convolution.h"
#include "math.h"

// Y = 0.114⋅B + 0.587⋅G + 0.299⋅R
double gray_matrix[] = {0.114, 0.587, 0.299};
double laplacian_filter[] = {0, 1, 0,
                             1,-4, 1,
                             0, 1, 0};

int Conv(img_info_t src_img, img_info_t* dst_img, kernel_t kernel)
{
    int ret_code;
    //padding, replicate
    img_info_t pad_img;
    ret_code = Padding(src_img, &pad_img, kernel);
    int pad_width       = pad_img.width;
    int pad_height      = pad_img.height;
    int pad_widthbyte   = pad_width*pad_img.channel;

    // convolution
    img_info_t out;
    out.width   = (pad_img.width - kernel.size) + 1;
    out.height  = (pad_img.height - kernel.size) + 1;
    out.channel = pad_img.channel;

    int out_width       = out.width;
    int out_height      = out.height;
    int nchannel        = out.channel;
    int out_widthbyte   = out_width*nchannel;

    double* dst = NULL;
    dst = (double*)malloc(out_widthbyte*out_height*PIXEL_SIZE);
    if( dst == NULL){
        printf("memory allocate fail. %s, %d", __FILE__, __LINE__);
        return -1;
    }

    //dst_img coordinate
    double sum = 0, d, k;
    for(int jloop = 0; jloop < out_height; jloop++){
        for(int iloop = 0; iloop < out_width; iloop++){
            for(int nch = 0; nch < nchannel; nch++){

                //convolution
                for(int j_k = 0; j_k < kernel.size; j_k++){
                    for(int i_k = 0; i_k < kernel.size; i_k++){
                        d = pad_img.data[(j_k+jloop)*pad_widthbyte + (i_k + iloop)*nchannel + nch];
                        k = kernel.matrix[j_k*kernel.size + i_k];
                        sum = sum + d*k;
                    }
                }
                dst[jloop*out_widthbyte + iloop*nchannel + nch] = (abs(sum) / kernel.scale);
                sum = 0;
            }//nch loop
        }//iloop
    }//jloop
    memcpy(dst_img, &out, sizeof(img_info_t));
    dst_img->data = dst;
    if(pad_img.data != NULL)
        free(pad_img.data);

    return ret_code;
}

//edge:replicate
int Padding(img_info_t src_img, img_info_t* dst_img, kernel_t kernel)
{
    int src_width       = src_img.width;
    int src_height      = src_img.height;
    int src_widthbyte   = src_width*src_img.channel;

    dst_img->width      = src_width + (kernel.size - 1);
    dst_img->height     = src_height + (kernel.size - 1);
    dst_img->channel    = src_img.channel;

    int dst_width       = dst_img->width;
    int dst_height      = dst_img->height;
    int dst_widthbyte   = dst_width*dst_img->channel;
    int nchannel        = dst_img->channel;
    int pad_thick       = (kernel.size - 1)/2;

    dst_img->data       = (double*)malloc(dst_widthbyte*dst_height*PIXEL_SIZE);
    if( dst_img->data == NULL){
        printf("memory allocate fail. %s, %d", __FILE__, __LINE__);
        return -1;
    }

    //padding img coordinate, fill with src value
    for (int jloop = 0; jloop < dst_height; jloop++){
        if( jloop < pad_thick || jloop > (dst_height - pad_thick-1))
            continue;

        memcpy(dst_img->data+ jloop*dst_widthbyte + pad_thick*nchannel, src_img.data+(jloop-pad_thick)*src_widthbyte, src_widthbyte*PIXEL_SIZE);
    }

    //top padding
    for (int jloop = 0; jloop < pad_thick; jloop++){
        memcpy(dst_img->data+ jloop*dst_widthbyte + pad_thick*nchannel, src_img.data, src_widthbyte*PIXEL_SIZE);
    }
    //button padding
    for (int jloop = (dst_height - pad_thick); jloop < dst_height; jloop++){
        memcpy(dst_img->data+ jloop*dst_widthbyte + pad_thick*nchannel, src_img.data+(src_img.height-1)*src_widthbyte, src_widthbyte*PIXEL_SIZE);
    }

    for (int jloop = 0; jloop < dst_height; jloop++){
        //left padding
        for (int iloop = 0; iloop < pad_thick; iloop++){
            for(int nch = 0; nch < nchannel; nch++){
                dst_img->data[jloop*dst_widthbyte + iloop*nchannel + nch] = dst_img->data[jloop*dst_widthbyte + pad_thick*nchannel + nch];
            }
        }
        //right padding
        for (int iloop = dst_width - pad_thick; iloop < dst_width; iloop++){
            for(int nch = 0; nch < nchannel; nch++){
                dst_img->data[jloop*dst_widthbyte + iloop*nchannel + nch] = dst_img->data[jloop*dst_widthbyte + ((dst_widthbyte-1*nchannel)-pad_thick*nchannel) + nch];
            }// nch
        }// iloop
    }// jloop

    return 0;
}

int img_add(img_info_t src1_img, img_info_t src2_img, img_info_t* dst_img)
{
    if (src1_img.channel != src2_img.channel) return -1;

    int src1_width       = src1_img.width;
    int src1_height      = src1_img.height;
    int src1_widthbyte   = src1_width*src1_img.channel;
    int src2_width       = src2_img.width;
    int src2_height      = src2_img.height;
    int src2_widthbyte   = src2_width*src2_img.channel;

    dst_img->width      = MINIMUM(src1_img.width, src2_img.width);
    dst_img->height     = MINIMUM(src1_img.height, src2_img.height);
    dst_img->channel    = src1_img.channel;

    int dst_width       = dst_img->width;
    int dst_height      = dst_img->height;
    int dst_widthbyte   = dst_width*dst_img->channel;
    int nchannel        = dst_img->channel;

    dst_img->data       = (double*)malloc(dst_widthbyte*dst_height*PIXEL_SIZE);
    if( dst_img->data == NULL){
        printf("memory allocate fail. %s, %d", __FILE__, __LINE__);
        return -1;
    }

    double B1, B2, G1, G2, R1, R2;
    for(int jloop = 0; jloop < dst_height; jloop++){
        for(int iloop = 0; iloop < dst_width; iloop++){
            B1 = src1_img.data[jloop*src1_widthbyte + iloop*nchannel];
            B2 = src2_img.data[jloop*src2_widthbyte + iloop*nchannel];
            dst_img->data[jloop*dst_widthbyte + iloop*nchannel] = B1 + B2;
            G1 = src1_img.data[jloop*src1_widthbyte + iloop*nchannel + 1];
            G2 = src2_img.data[jloop*src2_widthbyte + iloop*nchannel + 1];
            dst_img->data[jloop*dst_widthbyte + iloop*nchannel + 1] = G1 + G2;
            R1 = src1_img.data[jloop*src1_widthbyte + iloop*nchannel + 2];
            R2 = src2_img.data[jloop*src2_widthbyte + iloop*nchannel + 2];
            dst_img->data[jloop*dst_widthbyte + iloop*nchannel + 2] = R1 + R2;
        }
    }
    return 0;
}

int img_subs(img_info_t src1_img, img_info_t src2_img, img_info_t* dst_img)
{
    if (src1_img.channel != src2_img.channel) return -1;

    int src1_width       = src1_img.width;
    int src1_height      = src1_img.height;
    int src1_widthbyte   = src1_width*src1_img.channel;
    int src2_width       = src2_img.width;
    int src2_height      = src2_img.height;
    int src2_widthbyte   = src2_width*src2_img.channel;

    dst_img->width      = MINIMUM(src1_img.width, src2_img.width);
    dst_img->height     = MINIMUM(src1_img.height, src2_img.height);
    dst_img->channel    = src1_img.channel;

    int dst_width       = dst_img->width;
    int dst_height      = dst_img->height;
    int dst_widthbyte   = dst_width*dst_img->channel;
    int nchannel        = dst_img->channel;

    dst_img->data       = (double*)malloc(dst_widthbyte*dst_height*PIXEL_SIZE);
    if( dst_img->data == NULL){
        printf("memory allocate fail. %s, %d", __FILE__, __LINE__);
        return -1;
    }

    double B1, B2, G1, G2, R1, R2;
    for(int jloop = 0; jloop < dst_height; jloop++){
        for(int iloop = 0; iloop < dst_width; iloop++){
            B1 = src1_img.data[jloop*src1_widthbyte + iloop*nchannel];
            B2 = src2_img.data[jloop*src2_widthbyte + iloop*nchannel];
            dst_img->data[jloop*dst_widthbyte + iloop*nchannel] = B1-B2; //SATURATE_SUBS(B1,B2);//
            G1 = src1_img.data[jloop*src1_widthbyte + iloop*nchannel + 1];
            G2 = src2_img.data[jloop*src2_widthbyte + iloop*nchannel + 1];
            dst_img->data[jloop*dst_widthbyte + iloop*nchannel + 1] = G1-G2; //SATURATE_SUBS(G1, G2);//
            R1 = src1_img.data[jloop*src1_widthbyte + iloop*nchannel + 2];
            R2 = src2_img.data[jloop*src2_widthbyte + iloop*nchannel + 2];
            dst_img->data[jloop*dst_widthbyte + iloop*nchannel + 2] = R1-R2; //SATURATE_SUBS(R1, R2);//
        }
    }
    return 0;
}

int img_multi(img_info_t src1_img, img_info_t src2_img, int scale, img_info_t* dst_img)
{
    bool src2_valid = true;
    if (src2_img.data == NULL)
        src2_valid = false;
    else
        src2_valid = true;

    int src1_width, src1_height, src1_widthbyte, src1_channel;
    int src2_width, src2_height, src2_widthbyte, src2_channel;
    src1_width       = src1_img.width;
    src1_height      = src1_img.height;
    src1_channel     = src1_img.channel;
    src1_widthbyte   = src1_width*src1_channel;

    if(src2_valid){
        src2_width          = src2_img.width;
        src2_height         = src2_img.height;
        src2_channel        = src2_img.channel;
        src2_widthbyte      = src2_width*src2_channel;
        dst_img->width      = MINIMUM(src1_img.width, src2_img.width);
        dst_img->height     = MINIMUM(src1_img.height, src2_img.height);
        dst_img->channel    = MAXIMUM(src1_channel, src2_channel);
    }
    else{
        dst_img->width      = src1_width;
        dst_img->height     = src1_height;
        dst_img->channel    = src1_channel;
    }



    int dst_width       = dst_img->width;
    int dst_height      = dst_img->height;
    int dst_widthbyte   = dst_width*dst_img->channel;
    int dst_nchannel    = dst_img->channel;

    dst_img->data       = (double*)malloc(dst_widthbyte*dst_height*PIXEL_SIZE);

    double val1, val2;
    val1 = val2 = 1;
    for(int jloop = 0; jloop < dst_height; jloop++){
        for(int iloop = 0; iloop < dst_width; iloop++){
            for(int nch = 0; nch < dst_nchannel; nch++){
                if(1 == src1_channel)
                    val1 = src1_img.data[jloop*src1_widthbyte + iloop*src1_channel];
                else
                    val1 = src1_img.data[jloop*src1_widthbyte + iloop*src1_channel + nch];

                if(src2_valid){
                    val2 = src2_img.data[jloop*src2_widthbyte + iloop*src2_channel + nch];
                }
                dst_img->data[jloop*dst_widthbyte + iloop*dst_nchannel + nch] = val1*val2*scale;
            }// nch loop
        }// iloop
    }// jloop
    return -1;
}

int img_rgb2gray(img_info_t src_img, img_info_t* dst_img)
{
    int src_width       = src_img.width;
    int src_height      = src_img.height;
    int src_widthbyte   = src_width*src_img.channel;
    int nchannel        = src_img.channel;

    dst_img->width      = src_img.width;
    dst_img->height     = src_img.height;
    dst_img->channel    = 1;

    int dst_width       = dst_img->width;
    int dst_height      = dst_img->height;
    int dst_widthbyte   = dst_width*dst_img->channel;

    dst_img->data = (double* )malloc(dst_widthbyte*dst_height*PIXEL_SIZE);
    if( dst_img->data == NULL){
        printf("memory allocate fail. %s, %d", __FILE__, __LINE__);
        return -1;
    }

    double val = 0;
    for(int jloop = 0; jloop < dst_height; jloop++){
        for(int iloop = 0; iloop < dst_width; iloop++){
            val = src_img.data[jloop*src_widthbyte + iloop*nchannel    ]*gray_matrix[0]
                 +src_img.data[jloop*src_widthbyte + iloop*nchannel + 1]*gray_matrix[1]
                 +src_img.data[jloop*src_widthbyte + iloop*nchannel + 2]*gray_matrix[2];
            dst_img->data[jloop*dst_widthbyte + iloop] = val;
        }
    }

    return 0;
}

int img_contrast(img_info_t src_img, img_info_t* dst_img)
{
    int ret_code;
    img_info_t conv, gray;
    kernel_t filter;
    filter.size = 3;
    filter.scale = 1;
    ret_code = img_rgb2gray(src_img, &gray);
    memcpy(filter.matrix, laplacian_filter, sizeof(laplacian_filter));
    ret_code = Conv(gray, &conv, filter) + ret_code;
    memcpy(dst_img, &conv, sizeof(conv));

    if(gray.data != NULL)
        free(gray.data);

    return ret_code;
}

// method 0: standar diviation 1: RGB to HLS model
int img_saturation(img_info_t src_img, img_info_t* dst_img, int method)
{
    int src_width       = src_img.width;
    int src_height      = src_img.height;
    int src_widthbyte   = src_width*src_img.channel;
    int nchannel        = src_img.channel;

    dst_img->width      = src_img.width;
    dst_img->height     = src_img.height;
    dst_img->channel    = 1;

    int dst_width       = dst_img->width;
    int dst_height      = dst_img->height;
    int dst_widthbyte   = dst_width*dst_img->channel;

    dst_img->data = (double* )malloc(dst_widthbyte*dst_height*PIXEL_SIZE);
    if( dst_img->data == NULL){
        printf("memory allocate fail. %s, %d", __FILE__, __LINE__);
        return -1;
    }

    double B, G, R;
    double avg, SD;
    double Vmin, Vmax, C, L, temp;
    for(int jloop = dst_height-1; jloop > 0; jloop--){
        for(int iloop = dst_width-1; iloop > 0; iloop--){
            B = src_img.data[jloop*src_widthbyte + iloop*nchannel    ];
            G = src_img.data[jloop*src_widthbyte + iloop*nchannel + 1];
            R = src_img.data[jloop*src_widthbyte + iloop*nchannel + 2];

            switch(method){
                case 0:
                    avg = (B + G + R)/3;
                    SD = sqrt(((B-avg)*(B-avg) + (G-avg)*(G-avg) + (R-avg)*(R-avg)) /3);
                    dst_img->data[jloop*dst_widthbyte + iloop] = SD;
                    break;
                case 1:
                default:
                    temp = MAXIMUM(B, G);
                    Vmax = MAXIMUM(temp, R);
                    temp = MINIMUM(B, G);
                    Vmin = MINIMUM(temp, R);
                    C = Vmax - Vmin;
                    L = (Vmax + Vmin)/2;
                    if(0 == C)
                        dst_img->data[jloop*dst_widthbyte + iloop] = 0;
                    else
                        dst_img->data[jloop*dst_widthbyte + iloop] = C/(1-abs(2*L-1));
                    break;
            }// switch case
        }// iloop
    }// jloop

    return 0;
}

int well_exposedness(img_info_t src_img, img_info_t* dst_img)
{
    int src_width       = src_img.width;
    int src_height      = src_img.height;
    int src_widthbyte   = src_width*src_img.channel;
    int nchannel        = src_img.channel;

    dst_img->width      = src_img.width;
    dst_img->height     = src_img.height;
    dst_img->channel    = 1;

    int dst_width       = dst_img->width;
    int dst_height      = dst_img->height;
    int dst_widthbyte   = dst_width*dst_img->channel;

    dst_img->data = (double* )malloc(dst_widthbyte*dst_height*PIXEL_SIZE);
    if( dst_img->data == NULL){
        printf("memory allocate fail. %s, %d", __FILE__, __LINE__);
        return -1;
    }

    double sig = 0.2;
    double R, G, B, R1, G1, B1;
    for(int jloop = 0; jloop < dst_height; jloop++){
        for(int iloop = 0; iloop < dst_width; iloop++){
            B = src_img.data[jloop*src_widthbyte + iloop*nchannel    ];
            G = src_img.data[jloop*src_widthbyte + iloop*nchannel + 1];
            R = src_img.data[jloop*src_widthbyte + iloop*nchannel + 2];
            B1 = exp(-1*(B-0.5)*(B-0.5) / (2*sig*sig));
            G1 = exp(-1*(G-0.5)*(G-0.5) / (2*sig*sig));
            R1 = exp(-1*(R-0.5)*(R-0.5) / (2*sig*sig));
            dst_img->data[jloop*dst_widthbyte + iloop] = (B1*G1*R1);
        }
    }

    return 0;
}



