/* convolution.h
**
**
*/

#ifndef _CONVOLUTION_H_
#define _CONVOLUTION_H_

#include "common_define.h"

int Conv(img_info_t src_img, img_info_t* dst_img, kernel_t kernel);
int Padding(img_info_t src_img, img_info_t* dst_img, kernel_t kernel);

int img_add(img_info_t src_img, img_info_t src2_img, img_info_t* dst_img);
int img_subs(img_info_t src_img, img_info_t src2_img, img_info_t* dst_img);
int img_multi(img_info_t src1_img, img_info_t src2_img, int scale, img_info_t* dst_img);
int img_rgb2gray(img_info_t src_img, img_info_t* dst_img);

int img_contrast(img_info_t src_img, img_info_t* dst_img);
int img_saturation(img_info_t src_img, img_info_t* dst_img, int method);
int well_exposedness(img_info_t src_img, img_info_t* dst_img);

#endif