/*UD_sampling.h
**
**
*/
#ifndef _UD_SAMPLING_H_
#define _UD_SAMPLING_H_

#include "convolution.h"


//void test();
void test(img_info_t src_img,  img_info_t** dst_img);
int up_sampling(img_info_t src_img,  img_info_t* dst_img);
int down_sampling(img_info_t src_img,  img_info_t* dst_img);

#endif