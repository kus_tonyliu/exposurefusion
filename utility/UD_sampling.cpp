/* UD_sampling.cpp
** this file is a collection of implemetation of up / down sampling.
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include "UD_sampling.h"

double gaus_filter[] = { 1,  4,  6,  4, 1,
                        4, 16, 24, 16, 4,
                        6, 24, 36, 24, 6,
                        4, 16, 24, 16, 4,
                        1,  4,  6,  4, 1 };

int down_sampling(img_info_t src_img,  img_info_t* dst_img)
{
    int ret_code;
    img_info_t conv, src;
    kernel_t filter;
    filter.size = 5;
    filter.scale = 256;
    memcpy(filter.matrix, gaus_filter, sizeof(gaus_filter));
    ret_code = Conv(src_img, &conv, filter);
    memcpy(&src, &conv, sizeof(conv));

    int src_width   = src.width;
    int src_height  = src.height;

    dst_img->width      = (src.width+1) /2;
    dst_img->height     = (src.height+1) /2;
    dst_img->channel    = src.channel;
    dst_img->data       = (double*)malloc(dst_img->width*dst_img->height*dst_img->channel*PIXEL_SIZE);
    if( dst_img->data == NULL){
        printf("memory allocate fail. %s, %d", __FILE__, __LINE__);
        return -1;
    }

    int dst_width   = dst_img->width;
    int dst_height  = dst_img->height;
    int nchannel    = dst_img->channel;

    double *rmRow_img = NULL;
    rmRow_img = (double*)malloc(src_width*dst_height*nchannel*PIXEL_SIZE);
    if( rmRow_img == NULL){
        printf("memory allocate fail. %s, %d", __FILE__, __LINE__);
        return -1;
    }

    for(int jloop = 0; jloop < dst_height; jloop++){
        //remove even-row data, src -> rmRow
        memcpy(rmRow_img+jloop*src_width*nchannel, src.data+(jloop*2)*src_width*nchannel, src_width*nchannel*PIXEL_SIZE);

        //remove even-col data, rmRow -> dst
        for(int iloop = 0; iloop <dst_width; iloop++){
            dst_img->data[jloop*dst_width*nchannel + iloop*nchannel    ] =  rmRow_img[jloop*src_width*nchannel + (iloop*2)*nchannel    ];
            dst_img->data[jloop*dst_width*nchannel + iloop*nchannel + 1] =  rmRow_img[jloop*src_width*nchannel + (iloop*2)*nchannel + 1];
            dst_img->data[jloop*dst_width*nchannel + iloop*nchannel + 2] =  rmRow_img[jloop*src_width*nchannel + (iloop*2)*nchannel + 2];
        }
    }
    free(conv.data);

    return ret_code;
}

int up_sampling(img_info_t src_img,  img_info_t* dst_img)
{
    int ret_code;
    int src_width       = src_img.width;
    int src_height      = src_img.height;
    int src_bytewidth   = src_width*src_img.channel;

    dst_img->width      = src_img.width*2;
    dst_img->height     = src_img.height*2;
    dst_img->channel    = src_img.channel;

    int dst_width       = dst_img->width;
    int dst_height      = dst_img->height;
    int dst_bytewidth   = dst_width*dst_img->channel;
    int nchannel        = dst_img->channel;
    dst_img->data = (double*)malloc(dst_bytewidth*dst_height*PIXEL_SIZE);
    if( dst_img->data == NULL){
        printf("memory allocate fail. %s, %d", __FILE__, __LINE__);
        return -1;
    }
    memset(dst_img->data, 0, dst_bytewidth*dst_height*PIXEL_SIZE);

    double n1, n2, n3, n4;
    //src img coordinate
    for(int jloop = 0; jloop < src_height; jloop++){
        for(int iloop = 0; iloop < src_width; iloop++){
            dst_img->data[(jloop*2)*dst_bytewidth + (iloop*2*nchannel)    ] =  src_img.data[jloop*src_bytewidth + iloop*nchannel    ];
            dst_img->data[(jloop*2)*dst_bytewidth + (iloop*2*nchannel) + 1] =  src_img.data[jloop*src_bytewidth + iloop*nchannel + 1];
            dst_img->data[(jloop*2)*dst_bytewidth + (iloop*2*nchannel) + 2] =  src_img.data[jloop*src_bytewidth + iloop*nchannel + 2];

            if(iloop > 0){ //horizontall edge interpolation
                n1 = src_img.data[jloop*src_bytewidth + (iloop-1)*nchannel];
                n2 = src_img.data[jloop*src_bytewidth + iloop    *nchannel];
                dst_img->data[(jloop*2)*dst_bytewidth + (iloop*2-1)*nchannel] = (n1 + n2)/2 ;
                n1 = src_img.data[jloop*src_bytewidth + (iloop-1)*nchannel + 1];
                n2 = src_img.data[jloop*src_bytewidth + iloop    *nchannel + 1];
                dst_img->data[(jloop*2)*dst_bytewidth + (iloop*2-1)*nchannel + 1] = (n1 + n2)/2 ;
                n1 = src_img.data[jloop*src_bytewidth + (iloop-1)*nchannel + 2];
                n2 = src_img.data[jloop*src_bytewidth + iloop    *nchannel + 2];
                dst_img->data[(jloop*2)*dst_bytewidth + (iloop*2-1)*nchannel + 2] = (n1 + n2)/2 ;
            }

            if(jloop > 0){
                //vertical edge interpolation
                n1 = src_img.data[(jloop-1)*src_bytewidth + iloop*nchannel];
                n2 = src_img.data[jloop    *src_bytewidth + iloop*nchannel];
                dst_img->data[(jloop*2-1)*dst_bytewidth + (iloop*2)*nchannel] = (n1 + n2)/2 ;
                n1 = src_img.data[(jloop-1)*src_bytewidth + iloop*nchannel + 1];
                n2 = src_img.data[jloop    *src_bytewidth + iloop*nchannel + 1];
                dst_img->data[(jloop*2-1)*dst_bytewidth + (iloop*2)*nchannel + 1] = (n1 + n2)/2 ;
                n1 = src_img.data[(jloop-1)*src_bytewidth + iloop*nchannel + 2];
                n2 = src_img.data[jloop    *src_bytewidth + iloop*nchannel + 2];
                dst_img->data[(jloop*2-1)*dst_bytewidth + (iloop*2)*nchannel + 2] = (n1 + n2)/2 ;

                if(iloop > 0){
                    //cross postion interpolation
                    n1 = src_img.data[(jloop-1)*src_bytewidth + iloop    *nchannel];
                    n2 = src_img.data[jloop    *src_bytewidth + iloop    *nchannel];
                    n3 = src_img.data[(jloop-1)*src_bytewidth + (iloop-1)*nchannel];
                    n4 = src_img.data[(jloop)  *src_bytewidth + (iloop-1)*nchannel];
                    dst_img->data[(jloop*2-1)*dst_bytewidth + (iloop*2-1)*nchannel] = (n1 + n2 + n3 + n4)/4 ;
                    n1 = src_img.data[(jloop-1)*src_bytewidth + iloop    *nchannel + 1];
                    n2 = src_img.data[jloop    *src_bytewidth + iloop    *nchannel + 1];
                    n3 = src_img.data[(jloop-1)*src_bytewidth + (iloop-1)*nchannel + 1];
                    n4 = src_img.data[(jloop)  *src_bytewidth + (iloop-1)*nchannel + 1];
                    dst_img->data[(jloop*2-1)*dst_bytewidth + (iloop*2-1)*nchannel + 1] = (n1 + n2 + n3 + n4)/4 ;
                    n1 = src_img.data[(jloop-1)*src_bytewidth + iloop    *nchannel + 2];
                    n2 = src_img.data[jloop    *src_bytewidth + iloop    *nchannel + 2];
                    n3 = src_img.data[(jloop-1)*src_bytewidth + (iloop-1)*nchannel + 2];
                    n4 = src_img.data[(jloop)  *src_bytewidth + (iloop-1)*nchannel + 2];
                    dst_img->data[(jloop*2-1)*dst_bytewidth + (iloop*2-1)*nchannel + 2] = (n1 + n2 + n3 + n4)/4 ;
                }
            }// END of if(jloop > 0){


            //outter interpolation
            //button side
            if (jloop == src_img.height - 1){
                n1 = src_img.data[(jloop - 1) * src_bytewidth + iloop*nchannel];
                n2 = src_img.data[jloop       * src_bytewidth + iloop*nchannel];
                dst_img->data[(jloop*2 + 1) * dst_bytewidth + (iloop*2)*nchannel] = (3*n2 - n1)/2;
                n1 = src_img.data[(jloop - 1) * src_bytewidth + iloop*nchannel + 1];
                n2 = src_img.data[jloop       * src_bytewidth + iloop*nchannel + 1];
                dst_img->data[(jloop*2 + 1) * dst_bytewidth + (iloop*2)*nchannel + 1] = (3*n2 - n1)/2;
                n1 = src_img.data[(jloop - 1) * src_bytewidth + iloop*nchannel + 2];
                n2 = src_img.data[jloop       * src_bytewidth + iloop*nchannel + 2];
                dst_img->data[(jloop*2 + 1) * dst_bytewidth + (iloop*2)*nchannel + 2] = (3*n2 - n1)/2;
                //handle interval
                if(iloop > 0){
                    n1 = src_img.data[(jloop - 1) * src_bytewidth + iloop*nchannel];
                    n2 = src_img.data[(jloop)     * src_bytewidth + iloop*nchannel];
                    n3 = src_img.data[(jloop-1)   * src_bytewidth + (iloop-1)*nchannel];
                    n4 = src_img.data[(jloop)     * src_bytewidth + (iloop-1)*nchannel];
                    dst_img->data[(jloop*2 + 1)*dst_bytewidth + (iloop*2-1)*nchannel] = (3*n4 - n3 + 3*n2 - n1)/4;
                    n1 = src_img.data[(jloop - 1) * src_bytewidth + iloop*nchannel + 1];
                    n2 = src_img.data[(jloop)     * src_bytewidth + iloop*nchannel + 1];
                    n3 = src_img.data[(jloop-1)   * src_bytewidth + (iloop-1)*nchannel + 1];
                    n4 = src_img.data[(jloop)     * src_bytewidth + (iloop-1)*nchannel + 1];
                    dst_img->data[(jloop*2 + 1)*dst_bytewidth + (iloop*2-1)*nchannel + 1] = (3*n4 - n3 + 3*n2 - n1)/4;
                    n1 = src_img.data[(jloop - 1) * src_bytewidth + iloop*nchannel + 2];
                    n2 = src_img.data[(jloop)     * src_bytewidth + iloop*nchannel + 2];
                    n3 = src_img.data[(jloop-1)   * src_bytewidth + (iloop-1)*nchannel + 2];
                    n4 = src_img.data[(jloop)     * src_bytewidth + (iloop-1)*nchannel + 2];
                    dst_img->data[(jloop*2 + 1)*dst_bytewidth + (iloop*2-1)*nchannel + 2] = (3*n4 - n3 + 3*n2 - n1)/4;
                }
            }

            // right side
            if (iloop == src_width - 1){
                n1 = src_img.data[jloop * src_bytewidth + (iloop - 1)*nchannel];
                n2 = src_img.data[jloop * src_bytewidth + iloop*nchannel];
                dst_img->data[(jloop*2) * dst_bytewidth + (iloop*2 + 1)*nchannel] = (3*n2 - n1)/2;
                n1 = src_img.data[jloop * src_bytewidth + (iloop - 1)*nchannel + 1];
                n2 = src_img.data[jloop * src_bytewidth + iloop*nchannel + 1];
                dst_img->data[(jloop*2) * dst_bytewidth + (iloop*2 + 1)*nchannel + 1] = (3*n2 - n1)/2;
                n1 = src_img.data[jloop * src_bytewidth + (iloop - 1)*nchannel + 2];
                n2 = src_img.data[jloop * src_bytewidth + iloop*nchannel + 2];
                dst_img->data[(jloop*2) * dst_bytewidth + (iloop*2 + 1)*nchannel + 2] = (3*n2 - n1)/2;
                //handle interval
                if (jloop > 0){
                    n1 = src_img.data[(jloop)   * src_bytewidth + (iloop - 1)*nchannel];
                    n2 = src_img.data[(jloop)   * src_bytewidth + (iloop)    *nchannel];
                    n3 = src_img.data[(jloop-1) * src_bytewidth + (iloop - 1)*nchannel];
                    n4 = src_img.data[(jloop-1) * src_bytewidth + (iloop)    *nchannel];
                    dst_img->data[(jloop*2-1) * dst_bytewidth + (iloop*2 + 1)*nchannel] = (3*n4 - n3 + 3*n2 - n1)/4;
                    n1 = src_img.data[(jloop)   * src_bytewidth + (iloop - 1)*nchannel + 1];
                    n2 = src_img.data[(jloop)   * src_bytewidth + (iloop)    *nchannel + 1];
                    n3 = src_img.data[(jloop-1) * src_bytewidth + (iloop - 1)*nchannel + 1];
                    n4 = src_img.data[(jloop-1) * src_bytewidth + (iloop)    *nchannel + 1];
                    dst_img->data[(jloop*2-1) * dst_bytewidth + (iloop*2 + 1)*nchannel + 1] = (3*n4 - n3 + 3*n2 - n1)/4;
                    n1 = src_img.data[(jloop)   * src_bytewidth + (iloop - 1)*nchannel + 2];
                    n2 = src_img.data[(jloop)   * src_bytewidth + (iloop)    *nchannel + 2];
                    n3 = src_img.data[(jloop-1) * src_bytewidth + (iloop - 1)*nchannel + 2];
                    n4 = src_img.data[(jloop-1) * src_bytewidth + (iloop)    *nchannel + 2];
                    dst_img->data[(jloop*2-1) * dst_bytewidth + (iloop*2 + 1)*nchannel + 2] = (3*n4 - n3 + 3*n2 - n1)/4;
                }
            }

            //last pixel
            if (iloop == src_width-1 && jloop == src_img.height-1) {
                n1 = src_img.data[(jloop-1) * src_bytewidth + (iloop - 1)*nchannel];
                n2 = src_img.data[jloop * src_bytewidth + iloop*nchannel];
                dst_img->data[(jloop*2 +1 ) * dst_bytewidth + (iloop*2 + 1)*nchannel] = (3*n2 - n1) / 2;
                n1 = src_img.data[(jloop-1) * src_bytewidth + (iloop - 1)*nchannel + 1];
                n2 = src_img.data[jloop * src_bytewidth + iloop*nchannel + 1];
                dst_img->data[(jloop*2 +1 ) * dst_bytewidth + (iloop*2 + 1)*nchannel + 1] = (3*n2 - n1) / 2;
                n1 = src_img.data[(jloop-1) * src_bytewidth + (iloop - 1)*nchannel + 2];
                n2 = src_img.data[jloop * src_bytewidth + iloop*nchannel + 2];
                dst_img->data[(jloop*2 +1 ) * dst_bytewidth + (iloop*2 + 1)*nchannel + 2] = (3*n2 - n1) / 2;
            }
        } // END of for(int iloop = 0; iloop < src_img.width; iloop++){
    }// END of for(int jloop = 0; jloop < src_img.height; jloop++){

    img_info_t conv, temp;
    kernel_t filter;
    filter.size = 5;
    filter.scale = 256;
    memcpy(filter.matrix, gaus_filter, sizeof(gaus_filter));
    memcpy(&temp, dst_img, sizeof(temp));
    ret_code = Conv(temp, &conv, filter);
    memcpy(dst_img, &conv, sizeof(conv));
    free(temp.data);

    return ret_code;
}


