/*pyramid.h
**
**
*/

#ifndef _GAUSSIAN_PYRAMID_H_
#define _GAUSSIAN_PYRAMID_H_

#include "common_define.h"

int gaussian_pyramid(img_info_t* src_img, int src_num,  img_info_t*** pyr_img, int *pyr_num);
int laplacian_pyramid(img_info_t** src_img, int pyr_num, int src_num, img_info_t*** pyr_img);
int reconstruct_laplacian_pyramid(img_info_t* src_pyr_img, int pyr_num, img_info_t** pyr_img);
int combine_Nsrc_pyr(img_info_t **wmap_gau_pyr, img_info_t **laplac_pyr, int pyr_num, int src_num, img_info_t **combin_pyr);
void check_img(img_info_t src_img);

#endif