#include <iostream>
#include <opencv2/opencv.hpp>
#include "utility/UD_sampling.h"
#include "pyramid.h"

void check_normalize(img_info_t* src_img, int src_num)
{
    int sum_img_width = src_img->width;
    int sum_img_height = src_img->height;
    int sum_img_widthbyte = sum_img_width*src_img->channel;
    double val = 0;

    //sum wmap to local variable
    for (int jloop = 0; jloop < sum_img_height; jloop++) {
        for (int iloop = 0; iloop < sum_img_width; iloop++) {
            for (int nloop = 0; nloop < src_num; nloop++) {
                    val = src_img[nloop].data[jloop*sum_img_widthbyte + iloop] + val;
            }// iloop

            if(val < 0.99 || val > 1.01){
                printf("weight is non normal.\n");
                printf("sum wmap x=%d, y=%d, val=%f\n", iloop, jloop, val);
            }
            val = 0;
        }// jloop
    }// nloop
}

int weight_normalize(img_info_t* src_img, int src_num)
{
    int sum_img_width = src_img->width;
    int sum_img_height = src_img->height;
    int sum_img_widthbyte = sum_img_width*src_img->channel;

    double* sum_img_data;
    sum_img_data = (double*)malloc(sum_img_widthbyte*sum_img_height*PIXEL_SIZE);
    if( sum_img_data == NULL){
        printf("memory allocate fail. %s, %d", __FILE__, __LINE__);
        return -1;
    }
    memset(sum_img_data, 0, sum_img_widthbyte*sum_img_height*PIXEL_SIZE);

    double val, val1;
    val = val1 = 0;

    //sum src_img to sum_img
    for (int jloop = 0; jloop < sum_img_height; jloop++) {
        for (int iloop = 0; iloop < sum_img_width; iloop++) {
            for (int nloop = 0; nloop < src_num; nloop++)  {
                val     = sum_img_data[jloop * sum_img_widthbyte + iloop] + 0.00000000001;// void total sum equals zero
                val1    = src_img[nloop].data[jloop * sum_img_widthbyte + iloop];
                sum_img_data[jloop * sum_img_widthbyte + iloop] = val + val1;
            } // nloop
        }// iloop
    }// jloop

    // normalize
    for (int jloop = 0; jloop < sum_img_height; jloop++) {
        for (int iloop = 0; iloop < sum_img_width; iloop++) {
            for (int nloop = 0; nloop < src_num; nloop++) {
                val = src_img[nloop].data[jloop*sum_img_widthbyte + iloop];
                src_img[nloop].data[jloop*sum_img_widthbyte + iloop] = val / sum_img_data[jloop*sum_img_widthbyte + iloop];
            }// nloop
        }// iloop
    }// jloop
    if(sum_img_data != NULL)
        free(sum_img_data);
    check_normalize(src_img, src_num);

    return 0;
}

int weight_map(img_info_t* src_img, int src_num, img_info_t** dst_img)
{
    int ret_code;
    img_info_t C, S, E, sum;
    img_info_t *wmap_normal_array;
    wmap_normal_array = (img_info_t*)malloc(src_num*sizeof(img_info_t));
    if( wmap_normal_array == NULL){
        printf("memory allocate fail. %s, %d", __FILE__, __LINE__);
        return -1;
    }

    for (int num = 0; num < src_num; num++){
        ret_code = img_contrast(src_img[num], &C);
        ret_code = img_saturation(src_img[num], &S, 0) + ret_code;
        ret_code = well_exposedness(src_img[num], &E) + ret_code;
        ret_code = img_add(C, E, &sum) + ret_code;
        ret_code = img_add(sum, S, (wmap_normal_array+num)) + ret_code;

        if (C.data != NULL)
            free(C.data);
        if (S.data != NULL)
            free(S.data);
        if (E.data != NULL)
            free(E.data);
        if (sum.data != NULL)
            free(sum.data);
        if(ret_code < 0)
            return ret_code;
    }
    ret_code = weight_normalize(wmap_normal_array, src_num);
    *dst_img = wmap_normal_array;

    return ret_code;
}

int U8convertF32(cv::Mat src_img, img_info_t* dst_img)
{
    int src_width       = src_img.cols;
    int src_height      = src_img.rows;
    int src_widthbyte   = src_width*src_img.channels();
    int nchannel        = src_img.channels();

    dst_img->width      = src_width;
    dst_img->height     = src_height;
    dst_img->channel    = nchannel;

    int dst_width       = dst_img->width;
    int dst_height      = dst_img->height;
    int dst_widthbyte   = dst_width*dst_img->channel;

    dst_img->data = (double*)malloc(dst_widthbyte*dst_height*PIXEL_SIZE);
    if( dst_img->data == NULL){
        printf("memory allocate fail. %s, %d", __FILE__, __LINE__);
        return -1;
    }

    for(int jloop = 0; jloop < dst_height; jloop++){
        for(int iloop = 0; iloop < dst_width; iloop++){
            for(int ich = 0; ich < nchannel; ich++){
                dst_img->data[jloop*dst_widthbyte + iloop*nchannel+ich] = (double)src_img.data[jloop*dst_widthbyte + iloop*nchannel+ich]/255;
            }// ich
        }// iloop
    }// jloop

    return 0;
}

void F32convertU8(img_info_t src_img, cv::Mat* dst_img)
{
    int src_width       = src_img.width;
    int src_height      = src_img.height;
    int src_widthbyte   = src_width*src_img.channel;
    int nchannel        = src_img.channel;

    dst_img->cols      = src_width;
    dst_img->rows      = src_height;

    int dst_width       = dst_img->cols;
    int dst_height      = dst_img->rows;
    int dst_widthbyte   = dst_width*nchannel;

    double val;
    for(int jloop = 0; jloop < dst_height; jloop++){
        for(int iloop = 0; iloop < dst_width; iloop++){
            for(int ich = 0; ich < nchannel; ich++){
                val = src_img.data[jloop*dst_widthbyte + iloop*nchannel+ich];
                if(1 < val) {
                    val = 1;
                }
                if(val < 0) {
                    val = 0;
                }
                dst_img->data[jloop*dst_widthbyte + iloop*nchannel+ich] = (unsigned char)(val*255+0.5);
            }// ich
        }// iloop
    }// jloop
}

int main( int argc, char** argv )
{
    // load all image path into string
    std::string src_folder = "SOURCE_IMGES\\set1";
    std::vector<cv::String> filepath;
    cv::glob(src_folder+"\\*.jpg", filepath, false);
    int pyr_num, src_num, ret_code;
    src_num = filepath.size();

    img_info_t *src_img_array;
    img_info_t *wmap_img_array;

    src_img_array = (img_info_t*)malloc(src_num*sizeof(img_info_t));
    if( src_img_array == NULL){
        printf("memory allocate fail. %s, %d", __FILE__, __LINE__);
        return -1;
    }
    do{
        //load bracketed image
        cv::Mat srcImage;
        for (int src_n = 0; src_n < src_num; src_n++){
            srcImage = cv::imread(filepath[src_n]);
            if (!srcImage.data) {
                return 1;
            }
            // convert cv::MAT to user-defined img_info_t
            ret_code = U8convertF32(srcImage, src_img_array+src_n);
            if(ret_code){
                return 1;
            }
            //cv::imshow("srcImage", srcImage);
            srcImage.release();
        }

        // src_image make weight_map and gau_pyr
        img_info_t **wmap_gau_pyr;
        ret_code = weight_map(src_img_array, src_num, &wmap_img_array);
        if (ret_code < 0) break;

        ret_code = gaussian_pyramid(wmap_img_array, src_num, &wmap_gau_pyr, &pyr_num);
        if (ret_code < 0) break;

        // src_image make gau_pyr and laplac_pyr
        img_info_t **gau_pyr, **laplac_pyr;
        ret_code = gaussian_pyramid(src_img_array, src_num, &gau_pyr, &pyr_num);
        if (ret_code < 0) break;

        ret_code = laplacian_pyramid(gau_pyr, pyr_num, src_num, &laplac_pyr);
        if (ret_code < 0) break;

        // linear combination of laplac pyr and wmap pyr of several input images.
        img_info_t *combin_pyr, *reconstruct_pyr;
        ret_code = combine_Nsrc_pyr(wmap_gau_pyr, laplac_pyr, pyr_num, src_num, &combin_pyr);
        if (ret_code < 0) break;

        // reconstruct pyr as HDR img
        ret_code = reconstruct_laplacian_pyramid(combin_pyr, pyr_num, &reconstruct_pyr);
        if (ret_code < 0) break;

        cv::Mat dstImage(reconstruct_pyr[0].height, reconstruct_pyr[0].width, CV_8UC3);
        F32convertU8(reconstruct_pyr[0], &dstImage);
        cv::imshow("dstImage", dstImage);
        cv::imwrite(src_folder+"\\out\\set1.jpg", dstImage);
        cv::waitKey(0);
    }while(0);

    return 0;
}