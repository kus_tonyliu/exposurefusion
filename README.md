# ExposureFusion
To get HDR image, blending bracketed images.
**Without DL/NN method.**

# Result
* set1  
input image  
<img src="/set1/C.jpg" width="200"/> <img src="/set1/E.jpg" width="200"/> <img src="/set1/F.jpg" width="200"/>  
weight map  
<img src="/set1/out/wmap3.jpg" width="200"/> <img src="/set1/out/wmap5.jpg" width="200"/> <img src="/set1/out/wmap6.jpg" width="200"/>  
result image  
<img src="/set1/out/set1.jpg" width="600"/>  

* set3  
input image  
<img src="/set3/uploadsbracketed_photo_1b_0.jpg" width="200"/> <img src="/set3/uploadsbracketed_photo_1c.jpg" width="200"/> <img src="/set3/uploadsbracketed_photo_1d.jpg" width="200"/>  
weight map  
<img src="/set3/out/wmap1.jpg" width="200"/> <img src="/set3/out/wmap2.jpg" width="200"/> <img src="/set3/out/wmap3.jpg" width="200"/>  
result image  
<img src="/set3/out/set3.jpg" width="600"/>  

# Enviroment
* IDE : Visual Studio Code
* Compiler : gcc of MinGW
* External lib : opencv:3.4.8

# Describe
 Use external lib **OpenCV** and **C++** container to read imges only.  
 The others functions is written by C.

 Futhermore, this code is not complete enough. There are two parts of coding can be better.
 * software architecture :   
    it can be written more systematized, such as init(), start(), deinit() in main().
 * memory :   
    * add init() to allocate memory and use some global pointer to access.  
      it may will more convenience to pass images data.
    * add if() to check malloc is success.

above is self-suggestion.

# Usage
modify src_folder path as following in main()  
**input location** : SOURCE_IMGES\\set1  
**output location** : *input location* + \\out\\set1.jpg

compile and run.

# Tree
├set1  
│ └───out  
├set2  
│ └───out  
├set3  
│ └───out  
├set4  
│ └───out  
├set5  
│ └───out  
├set6  
│ └───out  
├ utility  
│ ├── convolution.cpp  
│ ├── convolution.h  
│ ├── UD_sampling.cpp  
│ ├── UD_sampling.h  
│ └── common_define.h  
├── pyramid.cpp  
├── pyramid.h  
└── ExposureFusion.cpp  

# Algorithm flow
![flow](/workflow.jpg)


# Reference
* Exposure Fusion
    * paper  
    Tom Mertens, Jan Kautz, Frank Van Reeth, Exposure Fusion, In proceedings of Pacific Graphics 2007
    * website  
    https://mericam.github.io/exposure_fusion/index.html

*   matlab  
https://blogs.mathworks.com/steve/2019/04/09/multiresolution-image-pyramids-and-impyramid-part-2/?s_tid=blogs_rc_2


* OpenCV document
    * pyramids tutorial  
    https://docs.opencv.org/3.4/d4/d1f/tutorial_pyramids.html
    * color conversion  
    https://docs.opencv.org/3.4/de/d25/imgproc_color_conversions.html

* Laplacian Pyramid  
P. Burt and T. Adelson. The Laplacian Pyramid as a Compact Image Code. IEEE Transactions on Communication,COM-31:532–540, 1983.

* good explanation of Laplacian Pyramid blog  
https://paperswithcode.com/method/laplacian-pyramid

* enfuse, a freeware tool  
https://wiki.panotools.org/Enfuse

* test bracketed images source  
https://learnandsupport.getolympus.com/la-es/node/569